package TestRunner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/Features"},
        glue = {"src/test/java/StepDefinitions"},
        plugin = {"pretty","html:target/Cucumber-Report.html"}
)
public class RunCucumberTest {
}

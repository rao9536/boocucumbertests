package bindings;

import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

public class IEWebDriverManager extends DriverManager {
    @Override
    protected void createWebDriver() {
        InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
        // additional settings
        this.webDriver = new InternetExplorerDriver(internetExplorerOptions);
    }
}

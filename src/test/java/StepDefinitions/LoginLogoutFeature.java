package StepDefinitions;

import bindings.DriverManager;
import bindings.DriverManagerFactory;
import bindings.DriverType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;
import pages.LoginPage;
import pages.LogoutPage;


public class LoginLogoutFeature {
    DriverManager driverManager;
    WebDriver webDriver;
    LogoutPage logoutPage;
    LoginPage loginPage;

    @Given("^I am on login page$")
    public void iAmOnLoginPage() {
        driverManager = DriverManagerFactory.getDriverManager(DriverType.FIREFOX);
        webDriver = driverManager.getWebDriver();
        webDriver.get("https://bankofontario.herokuapp.com/");
    }

    @When("^I enter username as \"(.*)\"$")
    public void enterUserName(String username) {
        webDriver.findElement(By.id("email")).sendKeys(username);
    }

    @When("^I enter password as \"(.*)\"$")
    public void enterPassword(String password) {
        webDriver.findElement(By.id("password")).sendKeys(password);
    }

    @When("^I click login button")
    public void clickLogin() {
        loginPage = new LoginPage(webDriver);
        loginPage.clickLogin();
    }

    @Then("Login should pass")
    public void verifyLogin() {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(webDriver.getTitle().trim(), "Bank of Ontario");
    }

    @When("^Click Logout button")
    public void clickLogoutButton() {
        logoutPage = new LogoutPage(webDriver);
        logoutPage.clickLogout();
    }

    @Then("^User should be Logout")
    public void verifyLoginPage() {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(webDriver.getTitle().trim(), "Bank of Ontario");
        driverManager.quitWebDriver();

    }

}

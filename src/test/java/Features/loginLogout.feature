Feature: Login and Logout functionality test
  Background: User is on login page

#Scenario with AND
  Scenario:
    Given I am on login page
    When I enter username as "teststudio@gmail.com"
    And I enter password as "teststudio"
    And I click login button
    Then Login should pass
    And Click Logout button
    Then User should be Logout


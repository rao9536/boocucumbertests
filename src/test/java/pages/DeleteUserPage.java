package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DeleteUserPage {
    WebDriver webDriver;

    private final By deleteButton = By.xpath("//body[1]/div[3]/form[1]/div[1]/div[1]/ul[1]/li[2]/a[1]");
    private final By selectAndDeleteUserButton = By.xpath("//span[contains(text(),'Vitruvi')]/parent::td//preceding-sibling::td//input[@name='id']");

    public DeleteUserPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public void clickDelete() {
        webDriver.findElement(deleteButton).click();
    }

    public void selectAndDeleteUser() {
        webDriver.findElement(selectAndDeleteUserButton).click();
        webDriver.findElement(deleteButton).click();
    }
}

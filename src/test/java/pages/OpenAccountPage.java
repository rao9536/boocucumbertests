package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class OpenAccountPage {
    WebDriver webDriver;

    public OpenAccountPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    private final By openAccountButton = By.xpath("//body[1]/div[3]/form[1]/div[1]/div[1]/ul[1]/li[1]/a[1]");

    private final By firstNameTextBox = By.id("firstName");
    private final By lastNameTextBox = By.id("lastName");
    private final By userTypeDropdown = By.id("userType");
    private final By accountBalanceTextBox = By.id("balance");
    private final By minimumBalanceTextBox = By.id("minimumBalance");
    private final By initialBalanceTextBox = By.id("initialBalance");
    private final By accountTypeDropdown = By.id("accountType");
    private final By virtualCardDropdown = By.id("hasVirtualCreditCard");
    private final By streetNumberTextBox = By.id("streetNumber");
    private final By streetNameTextBox = By.id("streetName");
    private final By cityTextBox = By.id("city");
    private final By provinceTextBox = By.id("province");
    private final By countryTextBox = By.id("country");
    private final By postalCodeTextBox = By.id("postalCode");
    private final By emailTextBox = By.id("email");
    private final By passwordTextBox = By.id("password");

    private final By registerButton = By.xpath("//body/div[@id='wrapper']/div[@id='workbench']/div[1]/div[1]/form[1]/fieldset[1]/div[16]/div[1]/input[1]");


    public void clickOpenAccount() {
        webDriver.findElement(openAccountButton).click();
    }

    public void openAccount(String firstName, String lastName,String userType, int accountBalance, int minimumBalance, int initialBalance,String accountType,String virtualCard,
                            int streetNumber, String streetName, String city, String province, String country, String postalCode, String email,
                            String password) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setUserType(userType);
        this.setAccountBalance(accountBalance);
        this.setMinimumBalance(minimumBalance);
        this.setInitialBalance(initialBalance);
        this.setAccountType(accountType);
        this.setVirtualCard(virtualCard);
        this.setStreetNumber(streetNumber);
        this.setStreetName(streetName);
        this.setCity(city);
        this.setProvince(province);
        this.setCountry(country);
        this.setPostalCode(postalCode);
        this.setEmail(email);
        this.setPassword(password);
        this.clickRegister();
    }

    private void setFirstName(String firstName) {
        webDriver.findElement(firstNameTextBox).sendKeys(firstName);
    }

    private void setLastName(String lastName) {
        webDriver.findElement(lastNameTextBox).sendKeys(lastName);
    }

    private void setUserType(String userType) {
        Select selectUserTypeDropDown = new Select(webDriver.findElement(userTypeDropdown));
        selectUserTypeDropDown.selectByVisibleText(userType);
    }

    private void setAccountBalance(int accountBalance) {
        webDriver.findElement(accountBalanceTextBox).clear();
        webDriver.findElement(accountBalanceTextBox).sendKeys(String.valueOf(accountBalance));
    }

    private void setMinimumBalance(int minimumBalance) {
        webDriver.findElement(minimumBalanceTextBox).clear();
        webDriver.findElement(minimumBalanceTextBox).sendKeys(String.valueOf(minimumBalance));
    }

    private void setInitialBalance(int initialBalance) {
        webDriver.findElement(initialBalanceTextBox).clear();
        webDriver.findElement(initialBalanceTextBox).sendKeys(String.valueOf(initialBalance));
    }

    private void setAccountType(String accountType) {
        Select selectAccountTypeDropDown = new Select(webDriver.findElement(accountTypeDropdown));
        selectAccountTypeDropDown.selectByVisibleText(accountType);
    }

    private void setVirtualCard(String virtualCard) {
        Select selectVirtualCardDropDown = new Select(webDriver.findElement(virtualCardDropdown));
        selectVirtualCardDropDown.selectByVisibleText(virtualCard);
    }

    private void setStreetNumber(int streetNumber) {
        webDriver.findElement(streetNumberTextBox).sendKeys(String.valueOf(streetNumber));
    }

    private void setStreetName(String streetName) {

        webDriver.findElement(streetNameTextBox).sendKeys(streetName);
    }

    private void setCity(String city) {
        webDriver.findElement(cityTextBox).sendKeys(city);
    }

    private void setProvince(String province) {
        webDriver.findElement(provinceTextBox).sendKeys(province);
    }

    private void setCountry(String country) {
        webDriver.findElement(countryTextBox).sendKeys(country);
    }

    private void setPostalCode(String postalCode) {
        webDriver.findElement(postalCodeTextBox).sendKeys(postalCode);
    }

    private void setEmail(String email) {
        webDriver.findElement(emailTextBox).sendKeys(email);
    }

    private void setPassword(String password) {
        webDriver.findElement(passwordTextBox).sendKeys(password);
    }

    private void clickRegister() {
        webDriver.findElement(registerButton).click();
    }
}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    WebDriver webDriver;

    public LoginPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    private final By userNameTextBox = By.id("email");
    private final By passwordTextBox = By.id("password");
    private final By loginButton = By.xpath("//body/div[@id='wrapper']/div[@id='workbench']/div[1]/div[1]/form[1]/fieldset[1]/div[3]/div[1]/input[1]");

    public void login(String userName, String password) {
        this.setUserName(userName);
        this.setPassword(password);
        this.clickLogin();
    }

    public void setUserName(String userName) {
        webDriver.findElement(userNameTextBox).sendKeys(userName);
    }

    public void setPassword(String password) {
        webDriver.findElement(passwordTextBox).sendKeys(password);
    }

    public void clickLogin() {
        webDriver.findElement(loginButton).click();
    }
}